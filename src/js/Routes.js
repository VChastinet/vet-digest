import { Services } from './util/services';
import ClinicCollection from './collections/ClinicCollection';
import ClinicListView from './views/clinic-list/ClinicListView';
import DetailsView from './views/details/DetailsView';
import Clinic from './models/Clinic';
import search from './util/search';

const service = new Services();
const clinicCollection = new ClinicCollection();
const clinicListView = new ClinicListView({model: clinicCollection});
const clinicList = service.getClinicList();

export default Backbone.Router.extend({
  initialize: function() {
    Backbone.history.start({pushState: true});
  },
  routes: {
    'vet-digest/clinic/:id': 'details',
    'vet-digest/': 'main',
    '': 'main'
  },

  details: async function(id) {
    const singleClinic = await service.getClinic(id);
    const clinic = new Clinic(singleClinic);
    const detailsView = new DetailsView({model: clinic});
    detailsView.render();
  },
  
  main: async function() {
    const list = await clinicList;
    clinicCollection.add(list);
    clinicListView.render();
    search(clinicListView, list);
  }
});
