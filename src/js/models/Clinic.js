export default Backbone.Model.extend({
  defaults: {
    id: 0,
    name: 'name',
    address: 'address',
    postalCode: 'postal code',
    phone: 'phone',
    image: 'https://picsum.photos/200/150/?blur'
  }
});
