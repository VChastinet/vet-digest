import ClinicView from '../clinic/ClinicView';


export default Backbone.View.extend({
    model: {},
    tagName: 'div',
    className: 'row',
    seachClinic: function(name, list) {
        this.model.set(
            list.filter(clinic => {
                return clinic.name.toUpperCase().match(name.toUpperCase())
            }));
        this.render();
    },
    initialize: function() {
        return this.model.on('add', this.render, this);
    },
    render: function() {
        $('#header .btn').show()
        this.$el.html('');
        $('#app').html('');
        $('#app').append(this.$el);

        _.each(this.model.toArray(), clinic => {      
            this.$el.append((new ClinicView({model: clinic})).render().$el);
        });
        return this;
    }
});
