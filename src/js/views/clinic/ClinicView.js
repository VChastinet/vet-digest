import Clinic from '../../models/Clinic';
import ClinicTemplate from './clinic-template.html';
;

export default Backbone.View.extend({
    model: new Clinic(),
    tagName: 'div',
    className: 'col-md-4',
    initialize: function() {
        this.template = _.template(ClinicTemplate);        
    },
    events: {
		'click .clinic-card': 'getId',
    },
    getId: function() {
        return this.model.toJSON().id;
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});
