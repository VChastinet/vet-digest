import DetailsTemplate from './details-template.html';

export default Backbone.View.extend({
    model: {},
    tagName: 'aside',
    className: 'profile-card',
    initialize: function() {
        this.template = _.template(DetailsTemplate);        
    },
    render: function() {
        $('#header .btn').hide()
        $('#app').html('');
        this.$el.html(this.template(this.model.toJSON()));
        $('#app').append(this.$el);
        return this;
    }
});
