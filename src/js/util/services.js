export class Services {

  constructor() {
    this.list = [];
  }

  getClinicList() {
    return new Promise(resolve => {
      setTimeout(() => {
        $.ajax('assets/data.json')
          .done(response => {
            this.list = response;
            resolve(response)
          })
          .fail((xhr, error) => {
            console.warn(error);
            alert('Desculpe, ocorreu um erro.');
          })
          .always(() => $('#main-loading').hide());
      }, 1000);
    });
  }

  async getClinic(id) {
    return new Promise(resolve => {
      if (!this.list) {
        this.getClinicList().then(res => resolve(res.filter(clinic => clinic.id == id)[0]));
        return;
      }
      resolve(this.list.filter(clinic => clinic.id == id)[0]);
    });
  }
}
