import Clinic from '../models/Clinic';

export default Backbone.Collection.extend({
    model: Clinic,    
});