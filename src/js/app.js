import 'bootstrap';
import Routes from './Routes';


$(document).ready(
    (function(){
        const router = new Routes();
        $('body').on('click', 'a[href^="/"]', function(event) {            
            event.preventDefault();
            router.navigate($(this).attr('href'), {trigger: true});
        });
    })()
);
