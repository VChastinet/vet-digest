# The Vet Digest

## veterinary clinic list: test application for owlish job interview

## Rodando a aplicação:

    Para rodar aplicação, basta clonar o repositório e em seguinda, usando o terminal, rodar o comando "npm instal" para instalar as dependências necessárias. Por fim digite "npm start", ele irá iniciar projeto na porta 3250.

### A aplicação também estará disponível no link https://vchastinet.gitlab.io/vet-digest/
Para gerar uma build basta digitar no console: `npm run build`, os bundles serão gerados na pasta 'public.


## Funcionamento da aplicação:
  * A tela iniciação mostra cards com informações básicas de cada clínica e um botão no header para para buscar as clínicas por nome.
  
  * Ao clicar em um dos cartões, a aplicação o levará para a página específica do cartão clicado, onde mostrará todas as informações disponíveis do item escolhido, com uma opção para retornar à página inicial.

## Ferramentas:
Para a realização deste projeto foram utilizadas as seguintes ferramentas:
> * BackBone.  
> * Jquery.
> * Underscore.
> * Bootstrap.  
> * Webpack.

