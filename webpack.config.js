const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;

const plugins = [];
plugins.push(
  new CopyWebpackPlugin([{ from: 'src/assets', to: 'assets' }]),
  new MiniCssExtractPlugin({
    filename: '[name].[hash].css'
  }),
  new webpack.ProvidePlugin({
    $: 'jquery',
    Popper: ['popper.js', 'default'],
    _: 'underscore',
    Backbone: 'backbone'
  }),
  new HtmlWebpackPlugin({
    hash: true,
    excludeAssets: [/style.*.js/],
    template: path.join(__dirname, 'src', 'index.html'),
    filename: path.join(__dirname, 'public', 'index.html'),
    minify: {
      removeComments: true,
      collapseWhitespace: true,
      removeAttributeQuotes: true
    }
  }),
  new HtmlWebpackExcludeAssetsPlugin(),
  new CleanWebpackPlugin(['public']),
  new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i })
);

module.exports = {
  entry: {
    app: path.join(__dirname, 'src', 'js', 'app'),
    style: path.join(__dirname, 'src', 'css', 'style')
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].[hash].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015'],
            plugins: ['transform-object-rest-spread']
          }
        }
      },
      {
        test: /\.(css)$/,
        exclude: /(node_modules)/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('autoprefixer'),
                require('cssnano')({
                  preset: 'default'
                })
              ],
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            minimize: true,
            removeComments: false,
            collapseWhitespace: false,
            attrs: [':data-src']
          }
        }
      },
      {
        test: /\.(png|jpg|gif|svg|woff2?|eot|ttf|json)(\?.*)?$/,
        loader: 'file-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.css'],
    alias: {
      jquery: path.join(
        __dirname,
        'node_modules',
        'jquery',
        'dist',
        'jquery.min'
      ),
      underscore: path.join(
        __dirname,
        'node_modules',
        'underscore',
        'underscore-min'
      ),
      backbone: path.join(__dirname, 'node_modules', 'backbone', 'backbone-min')
    }
  },
  stats: {
    colors: true
  },
  plugins,
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    compress: false,
    historyApiFallback: {
      rewrites: [
        { from: /^\/$/, to: 'index.html' }
      ]
    },
    port: 3250
  }
};
